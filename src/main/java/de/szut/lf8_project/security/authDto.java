package de.szut.lf8_project.security;

import lombok.Data;

/**
 * Mappingobjekt für die Authentifizierung.
 */
@Data
public class authDto {
    private String access_token;
    private String expires_in;
    private String refresh_expires_in;
    private String refresh_token;
    private String token_type;
    private String session_state;
    private String not_before_policy;
    private String scope;
}
