package de.szut.lf8_project.security;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;

/**
 * Stellt Funktionen zur Authentifizierung
 */
public class JWTservice {
    /**
     * Holt sich den access_token.
     * @return The access_token
     */
    public String getTokenNew(){
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://keycloak.szut.dev/auth/realms/szut/protocol/openid-connect/token";
        LinkedMultiValueMap<String,String> postParams = new LinkedMultiValueMap<>();

        postParams.add("grant_type", "password");
        postParams.add("client_id", "employee-management-service");
        postParams.add("username", "user");
        postParams.add("password", "test");

        HttpEntity<LinkedMultiValueMap<String,String>> entity = new HttpEntity<LinkedMultiValueMap<String, String>>(postParams);
        ResponseEntity<authDto> response = restTemplate.exchange(url, HttpMethod.POST, entity, authDto.class);

        return response.getBody().getAccess_token();
    }
}