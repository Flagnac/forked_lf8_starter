package de.szut.lf8_project.Project;


import de.szut.lf8_project.Project.dto.GetProjectDTO;
import de.szut.lf8_project.Project.dto.PostProjectDTO;
import org.springframework.stereotype.Service;

/**
 * Mapperklasse für Objekte der Klasse Project.
 */
@Service
public class ProjectMapper {
    /**
     * Mapper für Projekt zu einem {@link PostProjectDTO}.
     *
     * @param project Das Projekt das zum  {@link PostProjectDTO} gemapped werden soll
     * @return Das  {@link PostProjectDTO} auf das gemapped wurde.
     */
    public PostProjectDTO projectToPostProjectDTO(ProjectEntity project) {
        PostProjectDTO mappedProject = new PostProjectDTO();
        mappedProject.setDescription(project.getDescription());
        mappedProject.setProjectManagerId(project.getProjectManagerId());
        mappedProject.setInvolvedEmployees(project.getInvolvedEmployees());
        mappedProject.setNeededQualification(project.getNeededQualification());
        mappedProject.setProjectName(project.getProjectName());
        mappedProject.setCustomerId(project.getCustomerId());
        mappedProject.setCustomerName(project.getCustomerName());
        mappedProject.setStart(project.getStart());
        mappedProject.setEndPlanned(project.getEndPlanned());
        mappedProject.setEndReal(project.getEndReal());

        return mappedProject;
    }

    /**
     * Mapper für Projekt zu einem {@link GetProjectDTO}.
     *
     * @param project Die {@link ProjectEntity} die zum {@link GetProjectDTO} gemapped werden soll
     * @return Das {@link GetProjectDTO} auf das gemapped wurde.
     */
    public GetProjectDTO projectToGetProjectDTO(ProjectEntity project) {
        GetProjectDTO mappedProject = new GetProjectDTO();

        mappedProject.setId(project.getId());
        mappedProject.setDescription(project.getDescription());
        mappedProject.setProjectName(project.getProjectName());
        mappedProject.setProjectManagerId(project.getProjectManagerId());
        mappedProject.setNeededQualification(project.getNeededQualification());
        mappedProject.setInvolvedEmployees(project.getInvolvedEmployees());
        mappedProject.setCustomerId(project.getCustomerId());
        mappedProject.setCustomerName(project.getCustomerName());
        mappedProject.setStart(project.getStart());
        mappedProject.setEndPlanned(project.getEndPlanned());
        mappedProject.setEndReal(project.getEndReal());

        return mappedProject;
    }

    /**
     * Mapper für {@link PostProjectDTO} um auf eine {@link ProjectEntity} zu mappen
     * @param dto Das {@link PostProjectDTO} dass auf die {@link ProjectEntity} gemapped werden soll
     * @return die {@link ProjectEntity} die aus {@link PostProjectDTO} generiert wurde
     */
    public ProjectEntity postProjectDtoToEntity(PostProjectDTO dto) {
        ProjectEntity entity = new ProjectEntity();

        entity.setDescription(dto.getDescription());
        entity.setProjectName(dto.getProjectName());
        entity.setEndPlanned(dto.getEndPlanned());
        entity.setEndReal(dto.getEndReal());
        entity.setProjectManagerId(dto.getProjectManagerId());
        entity.setNeededQualification(dto.getNeededQualification());
        entity.setInvolvedEmployees(dto.getInvolvedEmployees());
        entity.setCustomerName(dto.getCustomerName());
        entity.setCustomerId(dto.getCustomerId());
        entity.setStart(dto.getStart());

        return entity;
    }
}
