package de.szut.lf8_project.Project;

import de.szut.lf8_project.Exceptions.EntityNotKnownException;
import de.szut.lf8_project.Project.dto.GetProjectDTO;
import de.szut.lf8_project.Project.dto.PostProjectDTO;
import de.szut.lf8_project.exceptionHandling.ResourceNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("Store/Projects")
public class ProjectController {
    private final ProjectMapper mapper;
    private final ProjectService service;

    public ProjectController(ProjectMapper mapper, ProjectService service) {
        this.mapper = mapper;
        this.service = service;
    }

    @Operation(summary = "delivers a projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "one project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping("/{id}")
    public GetProjectDTO findById(@PathVariable final Long id){
        ProjectEntity projectEntity = this.service.readById(id);
        return this.mapper.projectToGetProjectDTO(projectEntity);
    }

    @Operation(summary = "creates a new Project with everything.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "created project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PostMapping
    public GetProjectDTO create(@RequestBody @Valid PostProjectDTO postProjectDTO) {
        ProjectEntity projectEntity = this.mapper.postProjectDtoToEntity(postProjectDTO);
        projectEntity = this.service.create(projectEntity);
        return this.mapper.projectToGetProjectDTO(projectEntity);
    }

    @Operation(summary = "delivers a list of projects")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "list of projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @GetMapping
    public List<GetProjectDTO> findAll() {
        return this.service
                .readAll()
                .stream()
                .map(e -> this.mapper.projectToGetProjectDTO(e))
                .collect(Collectors.toList());
    }

    @Operation(summary = "deletes a project by id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "delete successful"),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "resource not found",
                    content = @Content)})
    @DeleteMapping("/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable long id) {
        var entity = this.service.readById(id);
        this.service.delete(entity.getId());
    }

    @GetMapping("/ofEmployee/{id}")
    @Operation(summary = "gets all projects of an employee by his id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "found projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    public List<GetProjectDTO> findAllProjectsForSpecificEmployee(@PathVariable final Long id){
        List<Long> allProjectIds = this.service.findAllOfEmployeeId(id);
        List<GetProjectDTO> projectsOfEmployee = new ArrayList<>();
        ProjectEntity current;
        for(Long l : allProjectIds){
            current = service.readById(l);
            projectsOfEmployee.add(mapper.projectToGetProjectDTO(current));
        }
        return projectsOfEmployee;
    }

    @Operation(summary = "removes employee of a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "removed employee from project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PutMapping("/remove/{employeeId}/of/{projectId}")
    public GetProjectDTO removeEmployeeFromProject(@PathVariable final Long employeeId, @PathVariable final Long projectId){
        ProjectEntity updatedProject = this.service.removeEmployeeFromProject(projectId,employeeId);
        GetProjectDTO projectDto = this.mapper.projectToGetProjectDTO(updatedProject);
        return projectDto;
    }

    @Operation(summary = "removes employee of a project")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "removed employee from project",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    @PutMapping("/{id}")
    public GetProjectDTO updateProject(@RequestBody @Valid final PostProjectDTO updatedProject, @PathVariable final Long id){
        ProjectEntity result = this.service.updateProject(updatedProject, id);
        return this.mapper.projectToGetProjectDTO(result);
    }

    @PutMapping("/add/{employeeId}/to/{projectId}")
    @Operation(summary = "gets all projects of an employee by his id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "found projects",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetProjectDTO.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    public GetProjectDTO addEmployee(@PathVariable final Long employeeId, @PathVariable final Long projectId){
        ProjectEntity result = this.service.addEmployeeToProject(projectId, employeeId);
        return this.mapper.projectToGetProjectDTO(result);
    }
}
