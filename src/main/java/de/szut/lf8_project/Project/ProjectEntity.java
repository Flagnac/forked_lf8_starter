package de.szut.lf8_project.Project;

import de.szut.lf8_project.Employee.dto.GetEmployeeDto;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Klasse zum erstellen einer Entität
 */
@Data
@Entity
@Table(name = "Projects")
public class ProjectEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String description;

    private String projectName;

    private String projectManagerId;

    private String neededQualification;

    private String involvedEmployees;

    private String customerId;

    private String customerName;

    private String start;

    private String endPlanned;

    private String endReal;
}
