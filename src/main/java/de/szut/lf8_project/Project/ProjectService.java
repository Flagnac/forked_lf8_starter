package de.szut.lf8_project.Project;

import de.szut.lf8_project.Employee.EmployeeService;
import de.szut.lf8_project.Exceptions.EntityNotKnownException;
import de.szut.lf8_project.Exceptions.NotValidException;
import de.szut.lf8_project.Project.dto.PostProjectDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Service, der die CRUD-Befehle für die {@link ProjectEntity} enthält.
 */
@Service
public class ProjectService {
    private final ProjectRepository repository;
    private final EmployeeService serviceE;
    public ProjectService(ProjectRepository repository, EmployeeService serviceE) {
        this.repository = repository;
        this.serviceE = serviceE;
    }

    /**
     * Erstellt eine {@link ProjectEntity} nach dem Abbild einer übergebenen {@link ProjectEntity}
     *
     * @param entity Die {@link ProjectEntity}
     * @return Die erstellte {@link ProjectEntity}
     */
    public ProjectEntity create(ProjectEntity entity) throws EntityNotKnownException{
        if(isKnowEmployee(entity.getProjectManagerId())){
            return this.repository.save(entity);
        } else throw new EntityNotKnownException("Entity not known!");
    }

    /**
     * Gibt eine {@link ProjectEntity} abhängig von der Angegebenen <em>ID</em> zurück
     *
     * @param id Eine Zahl des Datentypes {@link Long}
     * @return Das Objekt der Klasse {@link ProjectEntity}
     */
    public ProjectEntity readById(Long id) throws  EntityNotKnownException{
        if(isKnowProject(id)){
            return this.repository.findById(id).get();
        } else throw new EntityNotKnownException("Entity not known!");
    }

    /**
     * Gibt alle {@link ProjectEntity}'s die gespeichert wurden zurück
     * @return Eine {@link List} von allen {@link ProjectEntity}
     */
    public List<ProjectEntity> readAll() {
        return this.repository.findAll();
    }

    /**
     * Löscht eine {@link ProjectEntity} abhängig von ver angegebenen <em>ID</em>
     * @param id Die <em>ID</em> der {@link ProjectEntity} die gelöscht werden soll
     */
    public void delete(Long id) throws EntityNotKnownException{
        if(isKnowProject(id)){
            this.repository.deleteById(id);
        } else throw new EntityNotKnownException("Entity not known!");
    }

    /**
     * Gibt eine Liste von <em>IDs</em> von {@link ProjectEntity} aus, in der ein <em>Employee</em> arbeitet.
     * @param id die <em>ID</em> des <em>Employees</me>, dessen {@link ProjectEntity} abgefragt werden sollen
     * @return Die Liste von allen <em>IDs</em> der {@link ProjectEntity} der ein bestimmter <em>Employee</em> zugewiesen ist
     */
    public List<Long> findAllOfEmployeeId(Long id) {
        List<ProjectEntity> allProjects = this.repository.findAll();
        List<Long> projectIdsOfEmployee = new ArrayList<>();
        for (ProjectEntity p : allProjects) {
            if (p.getInvolvedEmployees().contains(id.toString())) {
                projectIdsOfEmployee.add(p.getId());
            }
        }
        return projectIdsOfEmployee;
    }

    public ProjectEntity removeEmployeeFromProject(Long projectId, Long employeeId) throws EntityNotKnownException {
        if(isKnowProject(projectId)){
            ProjectEntity toUpdate = this.repository.findById(projectId).get();
            String[] involvedEmployees = toUpdate.getInvolvedEmployees().split(" ");
            int index;
            try{
                index = toUpdate.getInvolvedEmployees().indexOf(employeeId.toString());
            } catch (Exception e){
                return toUpdate;
            }
            index = (index+1)/2;
            StringBuilder sb = new StringBuilder("");
            for(int i = 0; i<involvedEmployees.length; i++){
                if(i == index){
                    continue;
                }else if(i == 0){
                    sb.append(involvedEmployees[i]);
                } else{
                    sb.append(" " + involvedEmployees[i]);
                }
            }
            toUpdate.setInvolvedEmployees(sb.toString());
            this.repository.save(toUpdate);
            return toUpdate;
        } else throw new EntityNotKnownException("Entity not known!");
    }

    public ProjectEntity updateProject(PostProjectDTO projectEntity, final Long id) throws EntityNotKnownException{
        if(isKnowProject(id)&&isKnowEmployee(projectEntity.getProjectManagerId())){
            ProjectEntity updatedProjectEntity = new ProjectEntity();
            updatedProjectEntity.setId(id);
            updatedProjectEntity.setProjectName(projectEntity.getProjectName());
            updatedProjectEntity.setInvolvedEmployees(projectEntity.getInvolvedEmployees());
            updatedProjectEntity.setProjectManagerId(projectEntity.getProjectManagerId());
            updatedProjectEntity.setNeededQualification(projectEntity.getNeededQualification());
            updatedProjectEntity.setCustomerId(projectEntity.getCustomerId());
            updatedProjectEntity.setCustomerName(projectEntity.getCustomerName());
            updatedProjectEntity.setStart(projectEntity.getStart());
            updatedProjectEntity.setEndPlanned(projectEntity.getEndPlanned());
            updatedProjectEntity.setEndReal(projectEntity.getEndReal());

            return this.repository.save(updatedProjectEntity);
        }
        else throw new EntityNotKnownException("Entity not known!");
    }

    public ProjectEntity addEmployeeToProject(final Long projectId, final Long employeeId){
        if(isKnowProject(projectId)&&isKnowEmployee(employeeId.toString())){
            if(this.serviceE.checkValidEmployeeAdd(employeeId,projectId) && this.serviceE.checkIfEmployeeBusy(employeeId) && this.serviceE.checkValidQualificationAdd(employeeId, projectId)){
                ProjectEntity entity = readById(projectId);
                entity.setInvolvedEmployees(entity.getInvolvedEmployees() + " " + employeeId);
                return entity;
            }else throw new NotValidException("Employee is not Valid");
        }else throw new EntityNotKnownException("Entity not known!");
    }

    private boolean isKnowProject(final Long id){
        try{
            this.repository.findById(id).get();
            return true;
        } catch (Exception e){
            return false;
        }
    }

    private boolean isKnowEmployee(final String id){
        try{
            this.serviceE.getEmployeeById(Long.parseLong(id));
            return true;
        } catch (Exception e){
            return false;
        }
    }

}
