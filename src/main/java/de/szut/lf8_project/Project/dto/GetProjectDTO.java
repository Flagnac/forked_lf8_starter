package de.szut.lf8_project.Project.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class GetProjectDTO {
    private Long id;

    private String description;

    private String projectName;

    private String projectManagerId;

    private String neededQualification;

    private String involvedEmployees;

    private String customerId;

    private String customerName;

    private String start;

    private String endPlanned;

    private String endReal;
}
