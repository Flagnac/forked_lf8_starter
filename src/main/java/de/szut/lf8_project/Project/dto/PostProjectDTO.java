package de.szut.lf8_project.Project.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;

@Data
public class PostProjectDTO {
    @NotBlank(message = "Should not be empty")
    private String description;

    @NotBlank(message = "Should not be empty")
    private String projectName;

    @NotBlank(message = "Should not be empty")
    private String projectManagerId;

    @NotBlank(message = "Should not be empty")
    private String neededQualification;

    @NotBlank(message = "Should not be empty")
    private String involvedEmployees;

    @NotBlank(message = "Should not be empty")
    private String customerId;

    @NotBlank(message = "Should not be empty")
    private String customerName;

    @NotBlank(message = "Should not be empty")
    private String start;

    @NotBlank(message = "Should not be empty")
    private String endPlanned;

    private String endReal;
}
