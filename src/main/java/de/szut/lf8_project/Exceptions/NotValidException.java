package de.szut.lf8_project.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class NotValidException extends RuntimeException{
    public NotValidException(String message){
        super(message);
    }
}
