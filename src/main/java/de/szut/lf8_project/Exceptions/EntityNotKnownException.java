package de.szut.lf8_project.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class EntityNotKnownException extends RuntimeException{
    public EntityNotKnownException(String message){
        super(message);
    }
}
