package de.szut.lf8_project.Employee.dto;

import lombok.Data;

@Data
public class AllEmployeesWithQualificationDto {
    private String designation;
    private GetEmployeeFromQualificationDto[] employees;
}
