package de.szut.lf8_project.Employee.dto;

import lombok.Data;

@Data
public class GetEmployeeDto {
    private int id;
    private String lastName;
    private String firstName;
    private String street;
    private int postcode;
    private String city;
    private String phone;
}
