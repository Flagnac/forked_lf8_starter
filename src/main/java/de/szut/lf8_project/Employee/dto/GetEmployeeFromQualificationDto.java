package de.szut.lf8_project.Employee.dto;

import lombok.Data;

@Data
public class GetEmployeeFromQualificationDto {
    private Long id;
    private String surname;
    private String firstname;
}
