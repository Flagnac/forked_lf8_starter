package de.szut.lf8_project.Employee.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class PostEmployeeDto {
    @NotBlank(message = "The firstname is mandatory!")
    private String firstName;

    @NotBlank(message = "The lastname is mandatory!")
    private String lastName;

    @NotBlank(message = "The full address is mandatory!")
    @Size(max = 50, message = "The street cant exceed 50 characters")
    private String street;

    @NotBlank(message = "The full address is mandatory!")
    @Size(max = 5, min = 5, message = "The postcode must have five characters!")
    private String postcode;

    @NotBlank(message = "The full address is mandatory!")
    @Size(max = 50, message = "The city cant exceed 50 characters")
    private String city;

    @NotBlank(message = "The phone number is mandatory!")
    private String phone;
}
