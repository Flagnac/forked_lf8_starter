package de.szut.lf8_project.Employee;

import de.szut.lf8_project.Employee.dto.GetEmployeeDto;
import de.szut.lf8_project.Employee.dto.PostEmployeeDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("Store/Employees")
public class EmployeeController {
    private final EmployeeService service;

    public EmployeeController(EmployeeService service) {
        this.service = service;
    }

    @PostMapping
    @Operation(summary = "posts an employee")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "posted the employee",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetEmployeeDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    public void postEmployee(@Valid @RequestBody final PostEmployeeDto postEmployeeDto){
        service.createEmployee(postEmployeeDto);
    }

    @GetMapping("/{id}")
    @Operation(summary = "gets an employee by his id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "found the employee",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetEmployeeDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    public GetEmployeeDto findEmployeeById(@PathVariable final Long id){
        GetEmployeeDto dto = service.getEmployeeById(id);
        return dto;
    }

    @GetMapping("/ofProject/{id}")
    @Operation(summary = "gets all employees of a project by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "found the employees",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = GetEmployeeDto.class))}),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted",
                    content = @Content),
            @ApiResponse(responseCode = "401", description = "not authorized",
                    content = @Content)})
    public List<GetEmployeeDto> findAllEmployeesOfProject(@PathVariable final Long id){
        return this.service.getAllEmployeesOfProject(id);
    }

    @DeleteMapping("/{id}")
    @Operation(summary = "deletes an employee by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "found the employee"),
            @ApiResponse(responseCode = "400", description = "invalid JSON posted"),
            @ApiResponse(responseCode = "401", description = "not authorized")})
    public void deleteEmployeeById(@PathVariable final Long id){
        this.service.deleteEmployee(id);
    }

}
