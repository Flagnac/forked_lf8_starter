package de.szut.lf8_project.Employee;

import de.szut.lf8_project.Employee.dto.AllEmployeesWithQualificationDto;
import de.szut.lf8_project.Employee.dto.GetEmployeeDto;
import de.szut.lf8_project.Employee.dto.GetEmployeeFromQualificationDto;
import de.szut.lf8_project.Employee.dto.PostEmployeeDto;
import de.szut.lf8_project.Exceptions.EntityNotKnownException;
import de.szut.lf8_project.Project.ProjectEntity;
import de.szut.lf8_project.Project.ProjectRepository;
import de.szut.lf8_project.Project.ProjectService;
import de.szut.lf8_project.security.JWTservice;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * Service zum Verwalten der Employees
 */
@Service
public class EmployeeService {
    private final RestTemplate restTemplate;
    private final String baseUrl = "https://employee.szut.dev";
    private final ProjectRepository repositoryPro;
    private final JWTservice jwtService;
    private String token;
    private HttpEntity entity;

    /**
     * Constructor für den Employer Service
     *
     * @param repositoryPro übergebenes Repository
     */
    public EmployeeService(ProjectRepository repositoryPro){
        this.repositoryPro = repositoryPro;
        this.jwtService = new JWTservice();
        try{
            this.token = jwtService.getTokenNew();
        } catch(Exception e){
            e.printStackTrace();
        }
        this.restTemplate = new RestTemplate();
        HttpHeaders header = new HttpHeaders();
        header.setBearerAuth(token);
        this.entity = new HttpEntity(header);
    }

    /**
     * Konsumiert einen API-Call ung gibt ein {@link GetEmployeeDto} zurück
     *
     * @param id id des Employees der zurückgegeben werden soll
     * @return ein {@link GetEmployeeDto}
     */
    public GetEmployeeDto getEmployeeById(Long id){
        try{
            ResponseEntity<GetEmployeeDto> response = restTemplate.exchange(baseUrl + "/employees/" + id, HttpMethod.GET, entity, GetEmployeeDto.class);
            return response.getBody();
        } catch (Exception e){
            throw new EntityNotKnownException("Employee not Found");
        }
    }

    /**
     * Konsumiert einen API-Call und erstellt eine Liste von {@link GetEmployeeDto}
     * @return
     *  Die erstellte Liste von allen Employees
     */
    public List<GetEmployeeDto> getAllEmployees(){
        ResponseEntity<GetEmployeeDto[]> response = restTemplate.exchange(
                                                        baseUrl + "/employees",
                                                        HttpMethod.GET,
                                                        entity,
                                                        GetEmployeeDto[].class);
        return Arrays.asList(Objects.requireNonNull(response.getBody()));
    }

    /**
     * Methode zum erstellen und speichern eines Employees
     * @param employeeToAdd
     *  Der Employee der gespeichert werden soll
     */
    public void createEmployee(PostEmployeeDto employeeToAdd){
        HttpHeaders header = new HttpHeaders();
        header.setBearerAuth(token);
        HttpEntity postEntity = new HttpEntity(employeeToAdd, header);
        restTemplate.exchange(baseUrl + "/employees", HttpMethod.POST, postEntity, PostEmployeeDto.class);
    }

    /**
     * Methode zum löschen eines Employees
     * @param id Die <em>ID</em> des zu löschenden Employees
     */
    public void deleteEmployee(Long id){
        restTemplate.delete(baseUrl + "/employees/" + id, entity);
    }

    /**
     * Erstellt eine Liste von Allen Employees, die in einem Projekt arbeiten
     * @param id Die <em>ID</> des
     * @return Die Erstellte Liste
     */
    public List<GetEmployeeDto> getAllEmployeesOfProject(Long id){
        ProjectEntity project = repositoryPro.findById(id).get();
        String[] allIds = project.getInvolvedEmployees().split(" ");
        List<GetEmployeeDto> allEmployees = new ArrayList<>();

        for(String s : allIds){
            allEmployees.add(getEmployeeById(Long.parseLong(s)));
        }
        return allEmployees;
    }

    /**
     * Checkt ob es den Mitarbeiter gibt.
     * @param employeeId
     * @param projectId
     * @return
     */
    public boolean checkValidEmployeeAdd(final Long employeeId, final Long projectId){
        try{
            getEmployeeById(employeeId);
        } catch (EntityNotKnownException e){
            return false;
        }
        return true;
    }

    /**
     * Hat der Mitarbeiter die nötige Qualifikation?
     * @param employeeId
     * @param projectId
     * @return
     */
    public boolean checkValidQualificationAdd(final Long employeeId, final Long projectId){
        try{
            ProjectEntity project = repositoryPro.findById(projectId).get();
            AllEmployeesWithQualificationDto qualificatedEmployees = restTemplate.exchange(
                    baseUrl + "/qualifications/" + project.getNeededQualification() + "/employees",
                    HttpMethod.GET,
                    entity,
                    AllEmployeesWithQualificationDto.class
            ).getBody();
            for(GetEmployeeFromQualificationDto dto : qualificatedEmployees.getEmployees()){
                if(dto.getId() == employeeId){
                    return true;
                }
            }
            return false;
        } catch(Exception e){
            return false;
        }
    }

    /**
     * Ist der Mitarbeiter schon an einem anderen Project beteildigt?
     * @param employeeId
     * @return
     */
    public boolean checkIfEmployeeBusy(final Long employeeId){
        List<ProjectEntity> allProjects = repositoryPro.findAll();
        StringBuilder sb = new StringBuilder();

        for(ProjectEntity p:allProjects){
            if(allProjects.indexOf(p) == 0){
                sb.append(p.getInvolvedEmployees());
            }else{
                sb.append(" " + p.getInvolvedEmployees());
            }
        }

        if(sb.toString().contains(employeeId.toString())){
            return false;
        }
        return true;

    }
}
